#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QSerialPort>
#include <QSerialPortInfo>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    char readChar();
    void writeChar(char ci);
    QString readL();
    void writeL(QString si);

private slots:
    void on_scanForPortsButton_clicked();
    void on_disconnectB_clicked();
    void on_connectB_clicked();

    void on_requestMessageB_clicked();

    void on_txLE_editingFinished();

    void on_clearPB_clicked();

    void readData();


private:
    Ui::MainWindow *ui;
    QSerialPort *m_serial = nullptr;
    //QByteArray txdata("/?!");
    //const QByteArray rxdata;
};

#endif // MAINWINDOW_H
