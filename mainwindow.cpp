#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "QDebug"
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    m_serial(new QSerialPort(this))
{
    ui->setupUi(this);
    connect(m_serial,SIGNAL(readyRead()),this,SLOT(readData()));

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_scanForPortsButton_clicked()
{
    ui->portsCB->clear();
    QString description;
    QString manufacturer;
    QString serialNumber;
    QString blankString = "";
    const auto infos = QSerialPortInfo::availablePorts();
    for (const QSerialPortInfo &info : infos) {
        QStringList list;
        description = info.description();
        manufacturer = info.manufacturer();
        serialNumber = info.serialNumber();
        list << info.portName()
             << (!description.isEmpty() ? description : blankString)
             << (!manufacturer.isEmpty() ? manufacturer : blankString)
             << (!serialNumber.isEmpty() ? serialNumber : blankString)
             << info.systemLocation()
             << (info.vendorIdentifier() ? QString::number(info.vendorIdentifier(), 16) : blankString)
             << (info.productIdentifier() ? QString::number(info.productIdentifier(), 16) : blankString);

        ui->portsCB->addItem(list.first(), list);
    }
    ui->portsCB->addItem(tr("Custom"));

    // fill up the baudrate combobox
    ui->baudRateCB->clear();
    ui->baudRateCB->addItem(QStringLiteral("1200"), QSerialPort::Baud1200);
    ui->baudRateCB->addItem(QStringLiteral("2400"), QSerialPort::Baud2400);
    ui->baudRateCB->addItem(QStringLiteral("4800"), QSerialPort::Baud4800);
    ui->baudRateCB->addItem(QStringLiteral("9600"), QSerialPort::Baud9600);
    ui->baudRateCB->addItem(QStringLiteral("19200"), QSerialPort::Baud19200);
    ui->baudRateCB->addItem(QStringLiteral("38400"), QSerialPort::Baud38400);
    ui->baudRateCB->addItem(QStringLiteral("115200"), QSerialPort::Baud115200);
    ui->baudRateCB->addItem(tr("Custom"));
    ui->baudRateCB->setCurrentIndex(3);

    // fill up the bits combobox
    ui->bitsCB->clear();
    ui->bitsCB->addItem(QStringLiteral("5"), QSerialPort::Data5);
    ui->bitsCB->addItem(QStringLiteral("6"), QSerialPort::Data6);
    ui->bitsCB->addItem(QStringLiteral("7"), QSerialPort::Data7);
    ui->bitsCB->addItem(QStringLiteral("8"), QSerialPort::Data8);
    ui->bitsCB->setCurrentIndex(3);

    // fill up the parity combobox
    ui->parityCB->clear();
    ui->parityCB->addItem(tr("None"), QSerialPort::NoParity);
    ui->parityCB->addItem(tr("Even"), QSerialPort::EvenParity);
    ui->parityCB->addItem(tr("Odd"), QSerialPort::OddParity);
    ui->parityCB->addItem(tr("Mark"), QSerialPort::MarkParity);
    ui->parityCB->addItem(tr("Space"), QSerialPort::SpaceParity);
    ui->parityCB->setCurrentIndex(0);

    // fill up direction control combobox
    ui->stopBitCB->clear();
    ui->stopBitCB->addItem(QStringLiteral("1"), QSerialPort::OneStop);
    ui->stopBitCB->addItem(QStringLiteral("2"), QSerialPort::TwoStop);
    ui->stopBitCB->setCurrentIndex(0);

    // fill up the stop bit combobox
    ui->dCCB->clear();
    ui->dCCB->addItem(tr("None"), QSerialPort::NoFlowControl);
    ui->dCCB->addItem(tr("RTS/CTS"), QSerialPort::HardwareControl);
    ui->dCCB->addItem(tr("XON/XOFF"), QSerialPort::SoftwareControl);
    ui->dCCB->setCurrentIndex(0);

    //ui->scanForPortsButton->setEnabled(false);
}

void MainWindow::on_disconnectB_clicked()
{
    if (m_serial->isOpen()){
        m_serial->close();
    }
    ui->statusLabel->setText(tr("Disconnected"));
    ui->connectB->setEnabled(true);
    //on_clearPB_clicked();
}

void MainWindow::on_connectB_clicked()
{
    QVariant vPortName = ui->portsCB->currentData(Qt::DisplayRole);
    QVariant vBaudRate = ui->baudRateCB->currentData(Qt::DisplayRole);
    QVariant vBits = ui->bitsCB->currentData(Qt::DisplayRole);
    QVariant vParity = ui->parityCB->currentData(Qt::DisplayRole);
    QVariant vStopBits = ui->stopBitCB->currentData(Qt::DisplayRole);
    QVariant vDC = ui->dCCB->currentData(Qt::DisplayRole);

    m_serial->setPortName(vPortName.toString());
    m_serial->setBaudRate(vBaudRate.toInt());
    m_serial->setDataBits(static_cast<QSerialPort::DataBits>(vBits.toInt()));
    m_serial->setParity(static_cast<QSerialPort::Parity>(vParity.toInt()));
    m_serial->setStopBits(static_cast<QSerialPort::StopBits>(vStopBits.toInt()));
    m_serial->setFlowControl(static_cast<QSerialPort::FlowControl>(vDC.toInt()));
    if (m_serial->open(QIODevice::ReadWrite)) {
        ui->statusLabel->setText(tr("Connected."));
    } else {
        ui->statusLabel->setText(tr("Error"));
    }
    ui->connectB->setEnabled(false);
}

void MainWindow::on_requestMessageB_clicked()
{
    QByteArray txdata("/?!\n");
    if(m_serial->isDataTerminalReady()){
        //m_serial->write(txdata);
        writeL("/?!");
        ui->rxTB->insertPlainText(txdata);
    }

    if(m_serial->isDataTerminalReady()){
        //QByteArray rxdata = m_serial->readAll();
        QString rxdata = readL();
        ui->rxTB->insertPlainText(rxdata);
    }
}

void MainWindow::on_txLE_editingFinished()
{
    QString txStringData(ui->txLE->text()+"\n");
    QByteArray txdata(txStringData.toUtf8());
    if(m_serial->isDataTerminalReady()){
        //m_serial->write(txdata);
        writeL(txStringData);
        ui->rxTB->insertPlainText(txdata);
    }
}

void MainWindow::on_clearPB_clicked()
{
    ui->txLE->clear();
    ui->rxTB->clear();
}

void MainWindow::readData(){

    //QByteArray rxdata = m_serial->readAll();
    QString rxdata = readL();
    ui->rxTB->insertPlainText(rxdata+"\n");
}

char MainWindow::readChar(){
    char c = 'x';
    //char y = '\n';

    if(m_serial->getChar(&c)){
        return c;
    }
    else{
        return ' ';
    }
}

QString MainWindow::readL(){
    QString so = "";
    bool contn = true;
    char c = ' ';
    int i = 0;  // char counter.
    int char_limit = 100;   // char limit

    while(contn && i<char_limit){
        i++;
        c = readChar();
        so += c;
        if(c == '\n'){
            contn = false;
            // test to know if it actually detects '\n'
            //so += "end";
            // it actually does!.
        }
    }
    return so;
}

void MainWindow::writeChar(char ci){
    m_serial->putChar(ci);
}

void MainWindow::writeL(QString si){
    QChar c = ' ';
    for (int i = 0;i<si.size();i++) {
        c = si.at(i);
        writeChar(c.toLatin1());
    }
    if(c != '\n'){
        writeChar('\n');
    }
}
